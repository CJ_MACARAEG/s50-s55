import CourseCard from '../components/CourseCard'
import Loading from '../components/Loading'
import {useEffect, useState} from 'react'
// import courses_data from '../data/courses' <- WE DON'T NEED ANYMORE

export default function Courses(){
	const [courses, setCourses] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect((isLoading) => {
		// Sets the Loading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(response => response.json())
		.then(result => {
			setCourses(
				result.map(course => {
					return (
						<CourseCard key={course._id} course={course}/>
					)
				})
			)
			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [])

	return(
			
				(isLoading) ?
					<Loading/>
			:
				<>
					{courses}
				</>
	)
}
